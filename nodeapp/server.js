os = require('os');

const express = require('express');

// Constants
const PORT = process.env.PORT || 8080;
const HOST = '0.0.0.0';

const APP_NAME = process.env.APPNAME || 'NodeApp';

const HOSTNAME = os.hostname();

const TRACE = process.env.TRACE || 0;


// App
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use((req, res, next) => {
  if (TRACE == 1) {
    console.log("Request received for '" + APP_NAME + "' on " + req.url + " [" + HOSTNAME + "]");
  }
  next();
})

app.get('/', (req, res) => {
  res.send("Hello from '" + APP_NAME + "' [" + HOSTNAME + "]\n");
});

app.get('/env', (req, res) => {
  let response = {};
  response.env = process.env;
  res.send(response);
});

app.listen(PORT, HOST, () => {
    console.log("Running '" + APP_NAME + "' on http://" + HOST + ":" + PORT + "");
  }
);
