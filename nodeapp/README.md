# Node App

## Pre requisites

Close this repository

```bash
REPO_URL=localhost:5000
docker build . -t $REPO_URL/nodeapp:v1
docker push $REPO_URL/nodeapp:v1

# Check
for APP in `curl -k -s -X GET http://$REPO_URL/v2/_catalog | jq ".repositories[]"`; do 
  echo APP:$APP; 
  echo $APP | xargs -I _ curl -k -s -X GET http://$REPO_URL/v2/_/tags/list | jq ".tags[]";
done
```

## Deploy on K8S

```bash
REPO_URL=localhost:5000

DEPLOY_NAME=nodeapp
kubectl run $DEPLOY_NAME --image $REPO_URL/nodeapp
kubectl expose deploy $DEPLOY_NAME --port 3080 --target-port 8080
```

or 

```bash
kubectl apply -f nodeapp-gen-dep.yaml
kubectl apply -f nodeapp-gen-svc.yaml
```

### Test

```bash
DEPLOY_NAME=nodeapp
CLUSTER_IP=`kubectl get svc | grep $DEPLOY_NAME | awk -F" " '{ print $3 }'`
CLUSTER_PORT=`kubectl get svc | grep $DEPLOY_NAME | awk -F" " '{ print $5 }' | cut -d'/' -f1`
echo K8S_CLUSTER: $CLUSTER_IP:$CLUSTER_PORT
curl -k http://$CLUSTER_IP:$CLUSTER_PORT/
```

### Increeaase replica set
```bash
kubectl scale deploy $DEPLOY_NAME --replicas=5
```

### Test with overridding APPNAME

```bash
DEPLOY_NAME=nodeapp-myapp1
CLUSTER_IP=`kubectl get svc | grep $DEPLOY_NAME | awk -F" " '{ print $3 }'`
CLUSTER_PORT=`kubectl get svc | grep $DEPLOY_NAME | awk -F" " '{ print $5 }' | cut -d'/' -f1`
echo K8S_CLUSTER: $CLUSTER_IP:$CLUSTER_PORT
curl -k http://$CLUSTER_IP:$CLUSTER_PORT/
```

```bash
DEPLOY_NAME=nodeapp-myapp2
CLUSTER_IP=`kubectl get svc | grep $DEPLOY_NAME | awk -F" " '{ print $3 }'`
CLUSTER_PORT=`kubectl get svc | grep $DEPLOY_NAME | awk -F" " '{ print $5 }' | cut -d'/' -f1`
echo K8S_CLUSTER: $CLUSTER_IP:$CLUSTER_PORT
curl -k http://$CLUSTER_IP:$CLUSTER_PORT/
```