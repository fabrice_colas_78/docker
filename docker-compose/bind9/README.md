## Set DNS on host

```sh
sudo vi /etc/docker/daemon.json

```

```json
{
  "dns" : [ "1.1.1.1", "1.0.0.1" ]
}

```
```sh
sudo systemctl restart docker

```

## Disable local DNS server (Ubuntu) and start Docker stack

```sh
# Disable local DNS
sudo sed -i 's/#DNSStubListener=yes/DNSStubListener=no/g' /etc/systemd/resolved.conf
sudo systemctl restart systemd-resolved

# Start stack
docker-compose up -d

```

## Links

* [BIND 9 Administrator Reference Manual](https://bind9.readthedocs.io/)
* [zone file](https://bind9.readthedocs.io/en/v9.18.21/chapter3.html#example-com-base-zone-file)